#!/bin/bash

# Set up a fresh Ubuntu Server instance to make sure it is ready
# for testing this project (may need to use sudo). Some of this
# stuff is probably already there on a vanilla Ubuntu install,
# but it never hurts to double-check.

apt update
apt upgrade
apt install gcc vim git

